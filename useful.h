#pragma once

#include <errno.h>
#include <stdint.h>
#include <assert.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdarg.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

#ifdef __clang__
	static inline void defer_cleanup(void (^*b)(void)) { (*b)(); }
    #define defer_merge(a,b) a##b
    #define defer_varname(a) defer_merge(defer_scopevar_, a)
    #define defer [[gnu::cleanup(defer_cleanup)]] void (^defer_varname(__COUNTER__))(void) = ^
#elifdef __GNUC__
	#define __DEFER__($fn, $V)      \
 	 auto void $fn(int*);         \
 	 [[gnu::cleanup($fn)]] int $V; \
 	 [[gnu::always_inline]] inline auto void $fn(int*)
	#define defer __DEFER(__COUNTER__)
	#define __DEFER(N) __DEFER_(N)
	#define __DEFER_(N) __DEFER__(__DEFER_FUNCTION_ ## N, __DEFER_VARIABLE_ ## N)
#endif

#define CLEANUP($fn_tail) [[gnu::cleanup(u_ ## cleanup_fn_ ## $fn_tail)]]
#define SEL_1($first, ...) $first,
#define SEL_2($first, $second, ...) $second,
#define SEL_3($first, $second, $third, ...) $third,
#define SEL_4($first, $second, $third, $fourth, ...) $fourth,
#define SEL_5($first, $second, $third, $fourth, $fifth, ...) $fifth,
#define SEL_1_SEMICOLON($first, ...) $first;
#define SEL_2_SEMICOLON($first, $second, ...) $second;
#define SEL_3_SEMICOLON($first, $second, $third, ...) $third;
#define SEL_4_SEMICOLON($first, $second, $third, $fourth, ...) $fourth;
#define STR_SEL_1($first, ...) #$first,
#define STR_SEL_2($_, $second, ...) #$second,
#define PLUS_ONE(...) + 1
#define u_kilobytes(value) ((value) * 1024LL)
#define u_megabytes(value) (kilobytes(value) * 1024LL)
#define u_gigabytes(value) (megabytes(value) * 1024LL)
#define u_terabytes(value) (gigabytes(value) * 1024LL)

#define CAT($this, $that) $this ## $that
#define XCAT($this, $that) CAT ($this, $that)

#define FORI($idx, $max) for (typeof ($max) $idx = 0; $idx < $max; $idx++)
#define FORI_SENT($idx, $arr) \
	for (ssize_t $idx = 0; ($arr)[$idx]; ($idx)++)
#define EACH($item, $size, $arr) \
    for (int keep = 1, \
            count = 0; \
        keep && count != $size; \
        keep = !keep, count++) \
      for (auto $item = ($arr) + count; keep; keep = !keep)
#define EACH_SENT($item, $arr) \
	for (typeof (($arr)[0]) *($item) = ($arr); *($item); ($item)++)
#define nod [[nodiscard]]
#define FIND_SENT($elem_name, $sent_finding_expr, $arr, $expr) ({ \
		typeof (*($arr)) *r = {}; \
		for (typeof (r) $elem_name = $arr; $sent_finding_expr; $elem_name++) { \
		if ($expr) \
		r = $elem_name; \
		} \
		r; \
		})

static inline void u_warn (char *fmt, ...) {

	// Use a function so we have something to set a breakpoint on.

	va_list ap;
	va_start (ap);
	vfprintf (stderr, fmt, ap);
	va_end (ap);
}
#define WARN($fmt, ...) \
	u_warn ($fmt __VA_OPT__(,) __VA_ARGS__)
#define WARN_OUT($fmt, ...) \
	do { \
		WARN ($fmt __VA_OPT__(,) __VA_ARGS__); \
		goto out; \
	} while (false)
#define WARN_RET($val, $fmt, ...) \
	do { \
		WARN ($fmt __VA_OPT__(,) __VA_ARGS__); \
		return $val; \
	} while (false)
#define WARN_FAIL($fmt, ...) \
	WARN_RET (FAIL, $fmt __VA_OPT__(,) __VA_ARGS__)
#define WARN_EXIT($code, $fmt, ...) \
	do { \
		WARN ($fmt __VA_OPT__(,) __VA_ARGS__); \
		exit ($code); \
	} while (false)
#if __STDC_VERSION__ >= 202311l
#define np nullptr
#else
#define np NULL
#endif
#define FIND($elem_name, $n_arr, $arr, $expr) ({ \
		typeof (*($arr)) *r = {}; \
		for (typeof (r) $elem_name = $arr; $elem_name < $arr + $n_arr; $elem_name++) { \
		if ($expr) \
		r = $elem_name; \
		} \
		r; \
		})

enum uerr {
	SUCC,
	FAIL,
	FAIL_BUT_OK,
};
typedef enum uerr uerr_e;
#define ASSERT assert
#define GROW($p, $n, $a, $amount_to_add, $incr_amount) \
	if ($n + $amount_to_add >= *($a)) { \
		*($a) += $incr_amount; \
		*($p) = realloc (*($p), *($a) * sizeof (typeof (**($p)))); \
		ASSERT (*$p); \
	}
#define REMOVE_FROM_ARR($array, $starting_from_idx, $n_to_remove, $max) \
	do { \
		memmove (&($array)[($starting_from_idx)], \
				&$array[($starting_from_idx) + ($n_to_remove)], \
				sizeof (typeof (*($array))) * \
				(*($max) - (($starting_from_idx) + ($n_to_remove)))); \
		(*($max))--;  \
	} while (0)
#define ADD($p, $n, ...) \
	do { \
		($p)[(*($n))] = __VA_ARGS__; \
		*($n) += 1; \
	} while (0)
#define	GROW_ADD($p, $n, $a, $amount_to_add, $incr_amount, ...) \
	({ \
		GROW ($p, *($n), $a, $amount_to_add, $incr_amount); \
		ADD (*($p), $n, __VA_ARGS__); \
		\
		/* Return the last item for your convenience. */ \
		&(*($p))[(*($n)) - 1]; \
	})
#define	GROW_ADD_EMPTY($p, $n, $a, $amount_to_add, $incr_amount) \
	GROW_ADD ($p, $n, $a, $amount_to_add, $incr_amount, (typeof ((*($p))[0])) {0})
#define DARR_GROW_ADD($darr, $amount, $incr_amount, ...) \
	GROW_ADD (&($darr)->d, &($darr)->n, &($darr)->cap, $amount, $incr_amount, __VA_ARGS__)
#define DARR_GROW_ADD_EMPTY($darr, $amount, $incr_amount, ...) \
	GROW_ADD (&($darr)->d, &($darr)->n, &($darr)->cap, $amount, $incr_amount, (typeof (($darr)->d[0])) {0})
// I know this is pretty pointless in C. Well, maybe I'll find a use
// for it. 2023-10-21T01:12:27+01:00: Slightly updated. What I had
// wouldn't have worked if you wanted to multiply. I'm sure you could
// do better than this.
#define u_reduce(_n_arr, _arr, _fn) ({ \
	__typeof (*_arr) r; \
	memset (&r, 0, sizeof r); \
	if (_n_arr) { \
		if (_n_arr > 1) { \
			r = _fn (&_arr[0], &_arr[1]); \
			for (auto i = 2; i < _n_arr; i++) \
				r = _fn (&r, &(_arr)[i]); \
		} else  \
			r = _arr[0]; \
	} \
	r; \
});

#define simple_reduce(_n_arr, _arr, _operator) ({ \
	__typeof (*_arr) r = _n_arr ? _arr[0] : 0; \
	for (int i = 1; i < _n_arr; i++)  \
		r = r _operator _arr[i]; \
	r; \
})

// Can make a version that takes a pointer and fills an out param with
// two vals.
#define u_minmax(_n_arr, _arr) \
	({ \
	 	typeof (*_arr) *r = (typeof (*_arr)[2]) {}; \
		if (!_n_arr) \
			r = 0; \
		else { \
			r[0] = r[1] = _arr[0]; \
			for (int i = 0; i < _n_arr; i++) { \
				r[0] = min (r[0], _arr[i]); \
				r[1] = max (r[1], _arr[i]); \
			} \
		} \
		/* You can return this r if you call this function with auto. Why? */ \
		r; \
	})

// Hello
#define u_grow(_p, _n, _a, _incr_amount) \
	if (_n >= _a) { \
		_a += _incr_amount; \
		_p = realloc (_p, _a * sizeof (typeof (*_p))); \
		assert (_p); \
	}

#define u_add(_p, _n, _a, _incr_amount, ...) ({ \
	u_grow (_p, _n, _a, _incr_amount); \
	\
	/* I'm not totally sure there's any point in */ \
	/* this __VA_ARGS__, but it probably doesn't */ \
	/* hurt. */ \
	(_p)[(_n)++] = (typeof (*(_p))) __VA_ARGS__; \
	&(_p)[(_n) - 1]; \
	})

#define $xstringify(s) str(s)
#define $stringify(s) #s

#define $n_digits_in_int(_int) (floor (log10 (abs (_int))) + 1)
#define $tf(_bool) _bool ? "true" : "false"

#define $nuke_ptr(_what) ({ \
	free ((void *) _what);  \
	_what = 0; \
})
#define $nuke_ptr_if(_what) ({ \
	if (_what) $nuke_ptr (_what); \
})
#define $nuke_ptr_arr_if(_arr, _n_arr) ({ \
	if (_arr) { \
		$fori (_i, _n_arr) \
			$nuke_ptr_if((_arr)[_i]); \
		$nuke_ptr (_arr); \
	} \
})
#define $nuke_ptr_arr(_arr, _n_arr) ({ \
	$fori (_i, _n_arr) \
		$nuke_ptr((_arr)[_i]); \
	$nuke_ptr (_arr); \
})
#define $get_member(_ptr, _offset, _size) \
	({ *(void **) ((void *) _ptr + _offset); })

#define $get_deets(_type, _name) \
	({ (size_t [2]) { offsetof (_type, _name), sizeof (_type) }; })

// Don't use this in a loop. If you're getting just one line, you
// don't care about the size, but you need the size when running in a
// loop, because it's how getline knows how to update the allocated
// size. Perhaps a better variation on this would be a function that
// takes struct { char *buf; size_t a; } you initialise to zero. It
// will malloc the first time and update the buffer after.
#define $get_one_line(buf, file) \
	({ \
	 	getline (buf, (ssize_t []) {0}, file); \
	})
#define $n_va_args(...)  (sizeof((int[]){0, ##__VA_ARGS__})/sizeof(int)-1)

#define u_snprintf(_buf, _fmt, ...) snprintf (*_buf, sizeof *(_buf), _fmt, ##__VA_ARGS__)

#define $fread_bytes(_ptr, _n, _stream) \
	fread (_ptr, sizeof *_ptr, _n, _stream);

// I'm casting nmemb to (size_t []) because it's dumb that lfind takes
// a pointer to the size.
#define $lfind(_key, _base, _nmemb, _compar) \
	lfind ((_key), (_base), (size_t []) {(_nmemb)}, sizeof *(_base), (_compar))

#define VA(_last_arg, _code) \
	({ \
		va_list ap; \
		va_start (ap, _last_arg); \
		auto _r = _code; \
		va_end (ap); \
		_r; \
	})v

#define u_find(key, arr, n, cmp) \
	({ \
	 	__typeof__ (arr[0]) *__r = 0; \
		auto _key = key; \
		u_each (p, n, arr) { \
			if (!cmp (&(_key), p)) { \
				__r = p; \
				break; \
			} \
		} \
		__r; \
	})

#define $simple_find(key, arr, n) \
	({ \
	 	__typeof__ (arr[0]) *_r = 0; \
		auto _key = key; \
		u_each (p, n, arr) { \
			if (*_key == *p) { \
				_r = p; \
				break; \
			} \
		} \
		_r; \
	})


#define u_find_sent(key, arr, cmp) \
	({ \
	 	__typeof__ (arr[0]) *_r = 0; \
		auto _key = key; \
		u_each_sent (p, arr) { \
			if (!cmp (&(_key), p)) { \
				_r = p; \
				break; \
			} \
		} \
		_r; \
	})

#define $simple_find_sent(key, arr) \
	({ \
	 	__typeof__ (arr[0]) *_r = 0; \
		auto _key = key; \
		u_each_sent (p, arr) { \
			if (*_key == *p) { \
				_r = p; \
				break; \
			} \
		} \
		_r; \
	})

// You can do "static $make_compar (name)". That's nice.
// I'll leave this in, but it's a bit silly. You're saving yourself typing
// this line. It'd be better if it also did "const void _type *a = (const
// void _type *) _a for you.
#define $make_compar(_name) \
	int _name (const void *_a, const void *_b)

#define $free_if(_obj) \
	({ if (_obj) free ((void *) _obj); })
#define $nuke_if(_obj) \
	({ if (_obj) { free ((void *) _obj); _obj = 0; }})

#define U_VO static 1

// Note that I do {0}. This means that it works on scalars, too, eg
// ints.
#define $zero_obj(ptr) \
	*ptr = (typeof (*ptr)) {0}

#define $zero_array(ptr, n) \
	memset (ptr, 0, sizeof *ptr * n)

#define $gtfo() \
	({ r = 1; goto out; })

#define $goto(where) \
	({ goto where; })

#define $gtfo_with_val(_val) \
	({ r = _val; goto out; })

#define $get_out(_name) \
	({ _name = 1; goto out; })

#define $get_out_with_val(_name, _val) \
	({ _name = _val; goto out; })

#define $free_arr(__arr, __n_arr) \
	({ \
		for (typeof (__n_arr) i = 0; i < __n_arr; i++) { \
			if (__arr[i]) \
				free (__arr[i]); \
		} \
	})

#define $not_within(val, min, max)													\
	(val < min || val >= max)

/* typedef float f32; */
/* typedef double f64; */

#define $swap(x, y) \
	do { \
		__typeof__ (x) z; \
		z = x; \
		x = y; \
		y = z; \
	} while (0);

// A naive imitation of other languages that return error values
// or what you want. "optionals", though there's nothing really
// optional about this. It's just a struct with two values.
#define $mkopt(type, name) \
	struct opt_##name { \
		int err; \
		union { \
			type t; \
			type; \
		}; \
	};

#define $highest_member(n_arr_, arr_, member_, member_type_) \
	({ \
	 	__typeof__ (arr_) res = 0; \
		member_type_ max_val = 0; \
		size_t offset = offsetof (__typeof__ (*arr_), member_); \
		u_each (a, arr_, n_arr_) { \
			char *p = (char *) a + offset; \
			member_type_ *member = (member_type_ *) p; \
			if (*member > max_val) { \
				max_val = u_max (max_val, *member); \
				res = a; \
			} \
		} \
		res; \
	})

#define u_find_by_member_r(arr, n_arr, val, compar, _member, arg) \
	({ \
	 	/* Note that it needs to be "__typeof__ (arr[0]) * res" or similar. */ \
	 	/* It can't just be __typeof__ (arr) because that might expand to */ \
	 	/* an array, which res can't return. */ \
	 	__typeof__ (*arr) *res = 0; \
		size_t offset = offsetof (__typeof__ (*(arr)), _member); \
		for (__typeof__ (*arr) *elem = (arr); \
				elem < (arr) + (n_arr); elem++) { \
			char *base = (char *) elem; \
			\
			__typeof__ ((*arr)._member) *p = ((void *) base + offset); \
			if (0 == ((compar) (val, p, arg))) { \
				res = elem; \
				break; \
			} \
		} \
		res; \
	})
#define u_find_by_member(arr, n_arr, val, compar, _member) \
	({ \
	 	/* Note that it needs to be "__typeof__ (arr[0]) * res" or similar. */ \
	 	/* It can't just be __typeof__ (arr) because that might expand to */ \
	 	/* an array, which res can't return. */ \
	 	__typeof__ (*arr) *res = 0; \
		size_t offset = offsetof (__typeof__ (*(arr)), _member); \
		\
		for (__typeof__ (*arr) *elem = (arr); \
				elem < (arr) + (n_arr); elem++) { \
			/* Here's an interesting, tricky thing, this __typeof__, concatenated \* \
			 * the struct. It's the only way to make it work. Has to be a pointer */ \
			auto p = (__typeof__ ((*arr)._member) *) (char *) elem + offset; \
			if (0 == ((compar) (val, p))) { \
				res = elem; \
				break; \
			} \
		} \
		res; \
	})
#define CONTAINER_OF(_ptr, _type, _memb) \
      ((_type *)((void *)(_ptr) - offsetof(_type, _memb)))

#define count_up_member(_arr, _n_arr, _member_offset, _member_type) 					\
	({ 																					\
	 	_member_type _res = 0; 															\
		for (__typeof__ (_arr[0]) *_p = _arr; _p < _arr + _n_arr; _p++) { 					\
			char *_p_offset = (char *) _p + _member_offset; 							\
			_member_type *_val = (_member_type *) _p_offset; 							\
			_res += *_val; 																\
		}; 																				\
		_res; 																			\
	})

#define $qsort(base, nmemb, compar) 													\
	qsort (base, nmemb, sizeof *base, compar);
#define $strmatch !strcmp
#define $strcasematch !strcasecmp
#define $strnmatch !strncmp
#define $strncasematch !strncasecmp
#define $plural(x) ((x) == 1 ? "" : "s")
#define $lambda(l_ret_type, l_args, l_body)    											\
	({ 																					\
	 	l_ret_type l_anonymous_f_name l_args											\
			l_body																		\
		&l_anonymous_f_name; 															\
	})

#define u_max($a, $b)  \
	({ __typeof__ ($a) a = ($a); \
	 __typeof__ ($b) b = ($b); \
	 a > b? a : b; })
#define u_min($a, $b) \
	({ __typeof__ ($a) a = ($a); \
	 __typeof__ ($b) b = ($b); \
	 a > b ? b : a; })

#define u_update_max($to_update, $new_val) do { \
	*($to_update) = u_max (*($to_update), $new_val); \
} while (0)
#define u_update_min($to_update, $new_val) do { \
	*($to_update) = u_min (*($to_update), $new_val); \
} while (0)

#define $clamp(_it, _bottom, _top) \
	({  u_min (_top, u_max (_bottom, _it)); })

// FIXME: get rid of this. There's no point; $fori is better. It
// gets the types right.
#define $for(_type, _idx, _max) 														\
	for (_type _idx = 0; _idx < _max; _idx++)
#define $forr(_type, _idx, _max) 														\
	for (_type _idx = _max - 1; _idx >= 0; _idx--)

// This __typeof__ (_max + 0) trick is to get rid of any const.
// I got it from https://stackoverflow.com/questions/18063373/is-it-possible-to-un-const-__typeof__-in-gcc-pure-c
#define $fori(_idx, _max) 																\
	for (__typeof__ (_max + 0) _idx = 0; _idx < _max; _idx++)
#define $forri(_idx, _max) 																\
	for (__typeof__ (_max + 0)  _idx = _max - 1; _idx >= 0; _idx--)

// Linked-list for
#define $forll(_elem, _ll) \
	for (auto (_elem) = _ll; (_elem); (_elem) = (_elem)->next)

#define U_OR(_code) ?: ({ _code; })

// Stops on a sentinal value, ie the zero at the end of a string.
#define u_each_sent(_item, _arr) \
	for (__typeof__ (_arr[0]) *(_item) = (_arr); *(_item); (_item)++)
#define u_each(_item, _n, _arr) \
	for (__typeof__ (_arr[0]) *(_item) = (_arr); (_item) < (_arr) + (_n); (_item)++)
#define u_seach($arr, $elem_name) \
	for (__typeof__ (($arr)->d[0]) *($elem_name) = ($arr)->d; \
			($elem_name) < ($arr)->d + ($arr)->count; ($elem_name)++)
#define $beach(_item, _arr, _n) \
	for (__typeof__ (_arr[0]) *(_item) = (_arr) + (_n) - 1; (_item) >= (_arr); (_item)--)

// Excuse this clusterfuck. It could be simpler. I just want it to be
// ssize_t. I've decided that signed ints are the way to go. Always.
#define u_len($x) ((ssize_t) ((sizeof($x)/sizeof(($x)[0])) / ((ssize_t)(!(sizeof($x) % sizeof(($x)[0]))))))
#define countof($x) ((ssize_t) ((sizeof($x)/sizeof(($x)[0])) / ((ssize_t)(!(sizeof($x) % sizeof(($x)[0]))))))

#define $__get_num(_res, str, e, expr) 													\
	char *end; 																			\
	errno = 0; 																			\
	_res = expr; 																		\
	errno || (e? end != e: *end != '\0')? 1: 0; 										\

#define $get_int(_res, str, e) 															\
	({ 																					\
		char *end; 																		\
		errno = 0; 																		\
		*_res = strtol (str, &end, 0); 													\
		/* If you've provided e, it's an error if you strtof doesn't parse */ 			\
		/* to e. If not, it's an error if you didn't parse anything */ 					\
		errno || (e? end != e: end == str)? 1: 0; 										\
	})

#define $get_int_from_binary(_res, str, e) 															\
	({ 																					\
		char *end; 																		\
		errno = 0; 																		\
		*_res = strtol (str, &end, 2); 													\
		errno || (e? end != e: end == str)? 1: 0; 										\
	})

#define $get_double(_res, str, e) \
	({ \
		char *end; \
		errno = 0; \
		*_res = strtod (str, &end); \
		errno || (e? end != e: end == str)? 1: 0; \
	})
#define $get_finite_double(_res, str, e) \
	({ \
	 	int rt = $get_double (_res, str, e); \
		if (!rt) \
			rt = !isfinite (*_res); \
		rt; \
	})

#define $get_float(_res, str, e) 														\
	({ 																					\
		char *end; 																		\
		errno = 0; 																		\
		*_res = strtof (str, &end); 														\
		/* If you've provided e, it's an error if you strtof doesn't parse */ 			\
		/* to e. If not, it's an error if you didn't parse anything */ 					\
		errno || (e? end != e: end == str)? 1: 0; 										\
	})

static inline bool is_power_of_two (u64 n) {
    return n > 0 && (n & (n - 1)) == 0;
}
size_t flag_i (int flag);
void *u_malloc (size_t amount);
void * u_calloc (size_t amount);
void *u_realloc(void *ptr, size_t amount);
static inline void u_cleanup_fn_free (void *arg) {
	free (* (void **) arg);
}
static inline void u_cleanup_fn_close_file (void *arg) {
	fclose (* (FILE **) arg);
}

static inline void u_newline () {
	putchar ('\n');
}
