#include "useful.h"
#include <assert.h>

size_t flag_i (int flag) {
	for (int i = 0; i < flag; i++)
		if (1 << i == flag) return i;
	assert (0);
}

void *u_malloc (size_t amount) {
	void *r = malloc (amount);
	assert (r);
	return r;
}
void * u_calloc (size_t amount) {
	/* */
	/* I can never ever ever remember the order of arguments, */
	/* so let's just make it like malloc */
	void *r = calloc (1, amount);
	assert (r);
	return r;
}
void *u_realloc(void *ptr, size_t amount) {
	void *r = reallocarray (ptr, 1, amount);
	assert (r);
	return r;
}
